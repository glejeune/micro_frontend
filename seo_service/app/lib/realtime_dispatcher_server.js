const debug = require('debug')('seo_service:server');
const WebSocket = require('ws');

let wss;

module.exports = {
  setup: server => {
    wss = new WebSocket.Server({server});
    wss.on('connection', (ws, req) => {
      debug('New connection on the websocket dispatcher!');
      ws.isAlive = true;

      ws.on('pong', () => {
        ws.isAlive = true;
      });

      ws.on('message', message => {
        wss.clients.forEach(ows => {
          if (ws != ows) {
            if (ows.isAlive) {
              ows.send(message);
            }
          }
        });
      });
    });

    setInterval(() => {
      wss.clients.forEach(ws => {
        if (!ws.isAlive) return ws.terminate();

        ws.isAlive = false;
        ws.ping(null, false, true);
      });
    }, 10000);
  },

  send: (topic, message) => {
    if (topic && message) {
      wss.clients.forEach(ws => {
        if (ws.isAlive) {
          ws.send(JSON.stringify({topic, message}));
        }
      });
    }
  },
};
