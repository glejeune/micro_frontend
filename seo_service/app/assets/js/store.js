function createStore(reducer, preloadedState = {}) {
  // Initialized State
  let state = preloadedState;
  const getState = () => state;

  // Initialized Listener list
  const listeners = [];
  const subscribe = listener => {
    // Save callback listener in our list
    listeners.push(listener);

    // return unsubscribe method
    return function unsubscribe() {
      const index = listeners.indexOf(listener);
      listeners.splice(index, 1);
    }
  };

  // Dispatch action by calling reducer method 
  // and call all listeners callback
  const dispatch = action => {
    state = reducer(state, action);
    listeners.forEach(listener => listener());
  }

  return { getState, subscribe, dispatch };
}
