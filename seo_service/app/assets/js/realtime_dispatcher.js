import EventEmitter from 'eventemitter3';

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16),
  );
}

class Topic extends EventEmitter {
  constructor(name) {
    super();

    this.name = name;
    this.subscribers = [];
    this.lastMessage = undefined;
  }

  subscribe(fun) {
    const subscriber = uuidv4();
    this.subscribers[subscriber] = fun;
    this.on(subscriber, fun);
    if (this.lastMessage) {
      this.emit(subscriber, this.lastMessage);
    }
    return subscriber;
  }

  unsubscribe(uuid) {
    const fun = this.subscribers[uuid];
    if (fun) {
      this.off(uuid, fun);
      delete this.subscribers[uuid];
    }
  }

  send(message) {
    this.lastMessage = message;
    Object.keys(this.subscribers).forEach(key => {
      this.emit(key, message);
    });
  }

  onError(error) {
    console.log(error);
  }
}

class RealtimeDispatcher {
  get LOCAL() {
    return 1;
  }

  get REMOTE() {
    return 2;
  }

  constructor() {
    this.topics = {};
    this.subscribers = {};
    this.ws = new WebSocket(`ws://${location.host}/socket.io/`);
    this.ws.onmessage = event => {
      const messageEvent = JSON.parse(event.data);
      this.send(messageEvent.topic, messageEvent.message, this.LOCAL);
    };
    this.ws.onerror = () => console.log('WebSocket error');
    this.ws.onopen = () => console.log('WebSocket connection established');
    this.ws.onclose = () => console.log('WebSocket connection closed');
  }

  subscribe(topicName, fun) {
    const topicObject = this.getTopic(topicName);
    const subscriber = topicObject.subscribe(fun);
    this.subscribers[subscriber] = topicName;

    return subscriber;
  }

  unsubscribe(subscriber) {
    const topicName = this.subscribers[subscriber];
    if (topicName) {
      this.getTopic(topicName).unsubscribe(subscriber);
      delete this.subscribers[subscriber];
    }
  }

  send(topicName, message, type = this.LOCAL) {
    if ((type & this.LOCAL) !== 0) {
      this.getTopic(topicName).send(message);
    }

    if ((type & this.REMOTE) !== 0) {
      if (this.ws) {
        setTimeout(() => {
          this._sendToWS(JSON.stringify({topic: topicName, message}));
        }, 100);
      }
    }
  }

  _sendToWS(message) {
    if (this.ws.readyState !== 1) {
      setTimeout(() => {
        this._sendToWS(message);
      }, 10);
    } else {
      this.ws.send(message);
    }
  }

  getTopic(topicName) {
    let topicObject = this.topics[topicName];
    if (!topicObject) {
      this.topics[topicName] = new Topic(topicName);
      topicObject = this.topics[topicName];
    }
    return topicObject;
  }
}

export default RealtimeDispatcher;
