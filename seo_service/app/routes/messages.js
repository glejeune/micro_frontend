const express = require('express');
const router = express.Router();
const dispatcher = require('../lib/realtime_dispatcher_server.js');

router.post('/', (req, res, next) => {
  dispatcher.send(req.body.topic, req.body.message);
  res.status(202).end();
});

module.exports = router;
