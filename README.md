## A very simple micro fontend example

Micro frontent example using :

  * [express](http://expressjs.com/)
  * [Rails](https://rubyonrails.org/)
  * [React](https://reactjs.org/)
  * [Phoenix Framework](https://phoenixframework.org/)
  * [RE:DOM](https://redom.js.org/)

### Let's play...

Juste run `docker-compose up`

Then go to http://localhost:8000/ (and http://localhost:1080 - login/mdp)

Now, try this :

```
curl -v -X POST -d '{"topic":"randomValue", "message": 1337}' -H "Content-Type: application/json" http://localhost:3000/messages
```
