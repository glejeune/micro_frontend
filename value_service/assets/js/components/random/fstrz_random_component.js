import {el} from 'redom';

class FstrzRandomComponent {
  constructor() {
    this.value = '?';
    this.el = el('span', this.value);
  }

  update() {
    this.el.textContent = this.value;
  }

  onmount() {
    const value = Math.floor(Math.random() * 100 + 1);

    if (window.RealTime) {
      window.RealTime.send(
        'randomValue',
        value,
        window.RealTime.LOCAL | window.RealTime.REMOTE,
      );

      this.randomValue = window.RealTime.subscribe('randomValue', e => {
        this.value = e;
        this.update();
      });
    }
  }

  onremount() {}

  onunmount() {
    this.randomValue && window.RealTime.unsubscribe(this.randomValue);
  }
}

export default FstrzRandomComponent;
